var tickerApp = angular.module('ticker', []);

    tickerApp.controller('tickerCtrl', ['$scope', '$timeout', '$interval', '$http', function($scope, $timeout, $interval, $http) {

        // $scope.items = [
        //     {title: 'Box 1'},
        //     {title: 'Box 2'},
        //     {title: 'Box 3'},
        //     {title: 'Box 4'},
        //     {title: 'Box 5'}
        // ];

        $http.get('/assets/js/test.json')
	    	.success(function(data, status, headers, config) {
	      		$scope.items = data;
	    })
	    	.error(function(data, status, headers, config) {
	      		console.log("Failure,...abort, abort, ABORT...");
	    });

        $scope.moving = false;

        $scope.moveDown = function() {
            $scope.moving = true;
            $timeout($scope.switchFirst, 4000);
            //$scope.switchFirst();
        };
        
        $scope.switchFirst = function() {
            $scope.moving = false;
        	$scope.items.unshift($scope.items.pop());
            $scope.$apply();
        };

        $interval($scope.moveDown, 4000);

    }]);


    //Directive to get the height elements
    tickerApp.directive('myHeightBind', function() {
      return {
        scope: {
          heightValue: '='
        },
        link: function(scope, element) {
          scope.$watch(function() {
            scope.heightValue = element.prop("offsetHeight");
          });
        }
      }
    });


    //Animate with jquery - meh... :(
    // tickerApp.directive('animate', function() {
    //     return {

    //         link: function(scope, element) {
    //             scope.$watch(function() {
    //                 $( ".in" ).slideDown( 1000, function() {
    //                     console.log('gata');
    //                 });
    //         });
    //     }
    //   }
    // });


    //Animate with jquery - meh^2... :(
    tickerApp.directive('animateTopBottom', ['$interval', function($interval) {
        return{
            restrict: 'A',
            link: function(scope, element, attrs){
                scope.$watch(function(){
                    //debugger;
                      $(".in").slideDown( 1000, function() {
                        //console.log("animation finished");
                      });
                });

                // var elementResult = element[0].getElementsByClassName("in");
                // console.log(elementResult);

                // scope.$watch(function() {
                //   //return element.attr("class");
                //   // return elementResult;
                //   // console.log(elementResult);
                // }, function() {
                //         //console.log(elementResult);
                //     });

                // attrs.$observe(function(){
                //     //debugger;
                //     $(".in").slideDown( 1000, function() {
                //     //console.log("animation finished");
                //     });
                // });

            }
        }
    }]);